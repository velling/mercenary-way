FROM python:3.6-alpine3.6

COPY requirements.txt /root/

RUN apk add --no-cache make && \
    pip3 install --no-cache-dir -r /root/requirements.txt && \
    rm -Rf /root/.cache && \
    rm -rf /var/cache/apk/* && \
    rm -f /root/requirements.txt

COPY make.bat index.rst conf.py Makefile /root/
COPY _themes /root/_themes
COPY chapters /root/chapters
RUN cd /root && \
    mkdir /result && \
    make html && \
    make epub && \
    mv _build/html /result && \
    mv _build/epub/sphinx.epub /result/mercenary.epub && \
    make clean && \
    rm -rf make.bat index.rst conf.py Makefile chapters _build _static _templates
